//
//  UniversityServiceTests.swift
//  Basic-iOS-AppTests
//
//  Created by Oktavia Citra on 06/08/22.
//

import XCTest
@testable import Basic_iOS_App

fileprivate protocol HTTPClient {
    func get(from url: URL)
}

fileprivate class HTTPClientSpy: HTTPClient {
    var requestedURLs: [URL] = []
    var errros: [Error] = []
    var messages: [(URL, Error)] = []
    
    func get(from url: URL) {
        requestedURLs.append(url)
    }
    
    // MARK: - Stubbing
    func complete(with error: Error) {
        let url = requestedURLs[0]
        messages.append((url, error))
    }
}

fileprivate class UniversityServiceImplementation {
    private let url: URL
    private let client: HTTPClient
    
    enum Error: Swift.Error {
        case connectivity
        case invalidData
    }
    
    init(_ url: URL, _ client: HTTPClient) {
        self.url = url
        self.client = client
    }
    
    func fetch(completion: UniversitiesHandler) {
        client.get(from: url)
        
        completion(.failure(.connectivity))
    }
    
}

typealias SUT = (UniversityServiceImplementation, HTTPClient)

class UniversityServiceTests: XCTestCase {
    
    func test_init_doesNotRequestDataFromURL() {
        let (_, client) = makeSUT()
        
        XCTAssertEqual(client.requestedURLs, [])
    }
    
    func test_requestDataFromURL_one() {
        let url = makeURL()!
        let (sut, client) = makeSUT()
        
        sut.fetchPosts { _ in }
        
        XCTAssertEqual(client.requestedURLs, [url])
    }
    
    func test_requestDataFromURL_twice() {
        let url = makeURL()!
        let (sut, client) = makeSUT()
        
        sut.fetchPosts { _ in }
        sut.fetchPosts { _ in }
        
        XCTAssertEqual(client.requestedURLs, [url, url])
    }
    
    // MARK: - Helpers
    
    func makeSUT(file: StaticString = #filePath, line: UInt = #line) -> SUT {
        let client = HTTPClientSpy()
        let url = makeURL()!
        let sut = UniversityServiceImplementation(url, client)
        
        trackMemoryLeaks(sut, file: file, line: line)
        
        return (sut, client)
    }
    
    func makeURL() -> URL? {
        var component = URLComponents()
        component.scheme = "http"
        component.host = "any-url.com"
        guard let url = component.url else { return nil }
        return url
    }

}
