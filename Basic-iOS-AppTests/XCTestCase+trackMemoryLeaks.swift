//
//  XCTestCase+trackMemoryLeaks.swift
//  Basic-iOS-AppTests
//
//  Created by Oktavia Citra on 07/08/22.
//

import XCTest

extension XCTestCase {
    func trackMemoryLeaks(_ instance: AnyObject, file: StaticString = #filePath, line: UInt = #line) {
        addTeardownBlock { [weak instance] in
            XCTAssertNil(instance, "Instance should have been deallocated. Potential memory leaks.", file: file, line: line)
        }
    }
}
