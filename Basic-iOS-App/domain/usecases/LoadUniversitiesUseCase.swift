//
//  LoadUniversitiesUseCase.swift
//  Basic-iOS-App
//
//  Created by Oktavia Citra on 05/08/22.
//

import Foundation

protocol LoadUniversitiesUseCase {
    func execute(with url: URL, completionHandler: @escaping UniversitiesHandler )
}

class DefaultLoadUniversitiesUseCase {
    private let repository: UniversityRepository
    
    init(_ repository: UniversityRepository) {
        self.repository = repository
    }
    
}
extension DefaultLoadUniversitiesUseCase: LoadUniversitiesUseCase {
    func execute(with url: URL, completionHandler: @escaping UniversitiesHandler ) {
        repository.fetchData(with: url) { result in
            switch result {
            case .success(let univesities):
                completionHandler(.success(univesities))
            case .failure(let error):
                completionHandler(.success(error))
            }
        }
    }
}
