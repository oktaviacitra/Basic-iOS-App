//
//  University.swift
//  Basic-iOS-App
//
//  Created by Oktavia Citra on 02/08/22.
//

import Foundation

struct University {
    var domains: [String]?
    var webPages: [String]?
    var stateProvince: String?
    var name: String?
    var country: String?
    var code: String?
}

extension University: Decodable {
    enum CodingKeys: String, CodingKey {
        case domains = "domains"
        case webPages = "web_pages"
        case stateProvince = "stateProvince"
        case name = "name"
        case country = "country"
        case code = "alpha_two_code"
    }
}
