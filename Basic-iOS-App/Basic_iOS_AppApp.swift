//
//  Basic_iOS_AppApp.swift
//  Basic-iOS-App
//
//  Created by Oktavia Citra on 02/08/22.
//

import SwiftUI

@main
struct Basic_iOS_AppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
