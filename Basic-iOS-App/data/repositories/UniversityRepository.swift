//
//  UniversityRepository.swift
//  Basic-iOS-App
//
//  Created by Oktavia Citra on 05/08/22.
//

import Foundation

protocol UniversityRepository {
    func fetchData(with url: URL, completionHandler: @escaping UniversitiesHandler )
}

final class DefaultUniversityRepository {
    private var universityService: UniversityService
    
    init(_ universityService: UniversityService) {
        self.universityService = universityService
    }
}

extension DefaultUniversityRepository: UniversityRepository {
    func fetchData(with url: URL, completionHandler: @escaping UniversitiesHandler ) {
        universityService.fetchData(with: url) { result in
            switch result {
            case .success(let univesities):
                completionHandler(.success(univesities))
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
}
