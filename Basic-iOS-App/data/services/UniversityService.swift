//
//  UniversityService.swift
//  Basic-iOS-App
//
//  Created by Oktavia Citra on 02/08/22.
//

import Foundation

typealias UniversitiesResult = Result<[University], Error>
typealias UniversitiesHandler = (UniversitiesResult) -> ()

public protocol UniversityService {
    func fetchData(with url: URL, completionHandler: @escaping UniversitiesHandler )
}

final class DefaultUniversityService {
    
    enum Error: Swift.Error {
        case unknown
        case notConnected
        case noData
        case failedToDecode
    }
}

extension DefaultUniversityService: UniversityService {
    func fetchData(with url: URL, completionHandler: @escaping UniversitiesHandler) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let _ = error {
                completionHandler(.failure(Error.unknown))
            } else if let response = response as? HTTPURLResponse, response.statusCode != 200 {
                completionHandler(.failure(Error.notConnected))
            } else if let data = data {
                do {
                    let decoded = try JSONDecoder().decode([University].self, from: data)
                    completionHandler(.success(decoded))
                } catch {
                    completionHandler(.failure(Error.failedToDecode))
                }
            }
        }
        .resume()
    }
    
}
